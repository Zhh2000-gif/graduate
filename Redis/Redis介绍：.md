Redis介绍：

1.哪些数据结构
    String   get/set
    Hash Hget/hset
    List  lpush/lpop
    Set Sadd/Smembers
    SortedSet Zadd/zrange
    HypeLogLog pfadd/pfcount

2.分布式锁
    先拿setnx来争抢锁，抢到后就用expire来设置一个过期时间以防止忘记
    一般set和expire合成一条指令来使用。

3.redis异步队列使用
    使用list类型，lpush生产，lpop消费，当lpop没有消息的时候，可以使用sleep，不用sleep，那就使用blpop，直到有消息来
    生产一次消费多次，使用pub/sub主题订阅模式
    延时队列使用：
        使用sortedSet，拿时间戳作为score，zadd生产消息，zrangebyscore来获取N秒之前的消息，轮询处理。

4.Redis如何实现持久化
    AOF和RDB配合使用，AOF类似于日志数据，RDB类似于一张全量的数据表。
    RDB的原理：fork和cow，创建子线程进行RDB操作，父子线程共享数据段，父线程读写服务出现脏的页面数据后，逐渐与子线程分离开。

5.缓存穿透  雪崩  击穿
    穿透是缓存中和数据库中都没有，解决：接口设置校验
    雪崩  大量的热点数据过期，解决：设置过期时间随机
    击穿 缓存中没有数据库中有，解决：布隆过滤器

6.布隆过滤器
    命令有bf.add和bf.madd 和bf.exits
    原理：使用bf.add的时候，会有一个很大的位数组和几个不一样无偏哈希函数(可以把哈希值算的比较随机)，对数组长度取模运算，得到不同的位置，
    再设置为1，取数据的时候如果有0就说明不存在。

7.高可用集群
    主从服务器的配置：
        设置三台服务器，从服务器的配置文件中配置replacaof  主机的ip地址，配置数据同步需要配置防火墙的端口
    哨兵模式：sentinel
        配置sentinel.conf文件，可以监控主机以及主机的从机，一般有多个哨兵监控主机，每次以固定频率发送信号确认
        是否存活，如果没有反应，就会加快发送频率来确认，多个哨兵确认主机宕机后就会进行主从切换。

     


