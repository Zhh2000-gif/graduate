## SSM

### Spring

入门：配置文件application.xml中的bean存放dao全限定类名，设置id和class，让spring容器来创建对象，在需要的地方用ApplicationContext app = new ClassPathXmlApplicationContext("applicationContext.xml"),app.getBean(“dao接口名")获取。

```
Spring配置文件：
	<bean>:
		id:唯一标识
		class:全限定类名，默认是通过 无参构造 实现的
			可以通过 工厂静态 工厂静态方法创建对象，Bean中配置class和Factory-method为创建对象的方法。
				   工工厂实例 先创建工厂的对象，再用工厂的对象的方法，factory-bean,两个bean。 
		scope:对象的作用范围
			默认为singleton单例 在加载容器时就创建对象
			prototype 多例 在getBean时才创建对象
        Bean生命周期：
            init-method=""
            destory-method=""
        Bean的依赖注入：Dao层注入到service层，在容器中完成对一个对象的依赖。
        	构造方法 有参通过bean中的constrator-args来实现
        	set方法 在service中写set方法，在bean的property设置name，ref
          依赖注入的数据类型：
          	普通的数据类型
          	引用数据类型
          	集合数据类型 <List><value></List>  <Map><entry></map>
	<import> 在application.xml引入其他模块依赖，
```

```
String相关API
ApplicationContext的实现类
1.ClassPathXmlApplicationContext从根目录下加载
2.FileSystemXmlApplication从磁盘上加载
3.AnntationConfigApplicationContext注解配置容器对象
获取对象两种方式：
getBean("id");
getBean(class);
配置外部的property文件
context：property-placeholder location=""
bean中用${key}来表示
```

```
注解开发
@Component 使用在类上代替原来的bean
@Controller 使用在web层上替代bean
@Service 使用在service层上
@Repository 使用在dao层上
@Autowired 使用在字段上用于根据类型依赖注入
@Qualifier 按照id的名称从Spring容器中匹配
@Resource(name="") 相当于@Autowired+@Qualifier
@Value("${key}") 从容器中获取值赋给变量
@Scope(singleton)
配置Bean扫描
context：component-scan base-package=""

Spring新注解
@Configuration 指定当前类是一个Spring配置类
@ComponentScan 扫描组件
@PropertySource 加载properties文件中的配置
@Bean 在方法上将返回值放在容器中，可以参数设置名字
@Import 导入其他的配置类

ContextLoaderListener获取应用上下文
```

### Spring mvc

```
请求过程：
请求到DispatcherServlet(前端控制器)->HandlerMapping处理映射器
					  			->HandlerAdaptor处理适配器(Controller)，返回ModleandView，然后解析视图。
@requestMapping(value  method  params)   
PlatformTransactionManager编程式事务控制相关对象
```

### AOP

```
面向切面编程，通过动态代理来实现功能的增强。
ps：静态代理是编译就已经把代理对象创建好，动态代理是在运行的时候创建代理对象。
jdk动态代理  代理的是接口
cglib动态代理  代理的可以为类和接口
```

```
Springfarmework的底层原理
类-->推断构造方法-->对象-->依赖注入(IOC)-->初始化前(Postprocessor)-->初始化(InilizationBean)-->初始化后(AOP)-->代理对象-->Bean

```

```
Bean的生命周期
1.解析类得到Beandefinition，即componentScan
2.推断使用的构造方法
3.创建对象后注入属性，@Autowired
4.回调Aware方法，BeanNameAware、BeanFactoryAware
5.初始化前
6.初始化
7.初始化后，Aop
8.单例或者原型创建bean
9.使用bean
10.destory
```

```
Spring中的设计模式
1.简单工厂
	BeanFactory就是用到了，根据传入的唯一标识来创建bean
2.单例模式
	提供了一个全局访问点BeanFactory，
3.适配器模式
	HandlerAdapter，适配Controller
4.装饰模式 额外增加功能，类名带有Decorator
5.动态代理 AOP
6.观察者模式 即监视器
```

```
Spring事务有编程式 声明式(@Transtractional)
事务的传播机制：多个事务方法互相调用时，事务如何在这些方法间传播，spring中默认的是required在@Transtracitonal注解的属性中。
事务失效：spring事务的原理是AOP，进行切面增强，
	1.发生自我调用，没有用到代理类
	2.数据库不支持
	3.方法不是public的
	4.没有被spirng管理
```

