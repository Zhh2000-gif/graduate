*1.cpu线程调度分为分时调度和抢占式调度
java虚拟机采用的是抢占式，优先级高的先使用cpu资源 

2.重进入是请求自己占有的锁，关联一个计数器和一个线程，当一个线程请求一个未占有的锁，计数加一，退出即减一。
例：子类覆写了父类的synchronized方法，没有可重入，则一直会等待同步方法，因为锁被占有。

3.线程池
通过Executors提供四种线程池
        newCachedThreadPool,线程池无限大，
        newFixedThreadPool.创建一个固定大小的线程池，可以控制线程的最大并发数
        newScheduledThreadPool,定长线程池 ，可以定时的执行任务
        newSingleThreadExecutor，单线程化的线程池
通过Executors.newCachedThreadPool来创建线程对象。
线程池的参数：
        1.corePoolSize  线程池核心线程大小
        2.maximumPoolSize  线程池最大线程的数量
        3.keepAliveTime   空闲线程的存活时间
        woekqueue  工作队列

4.自旋锁
一个线程在获取锁的时候，如果锁已经被其他线程获取   ，那么将循环等待，不断判断获取锁是否成功，
直到获取锁才会退出循环。如果一直获取不到，会设置一个次数，到达后即退出。

5.CAS算法思想，采用的乐观锁的思想。
CAS（V , E ,N）
V是需要更新的值
E是期望的值
N是新的值
比较V和E是否一样，如果一样，则说明没有其他线程修改，不一样则说明其他线程进行了修改，可以再次获取值来判断是否一样。

6.