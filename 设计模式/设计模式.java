//单例模式：
//饿汉式
public class Singleton1{
    //构造方法私有化
    private Singleton1(){}
    private static Singleton1 single = new Singleton1();
    //静态工厂方法
    public static Singleton1 getInstance(){
        return single;
    }
}
//懒汉式
public class Singleton2{
    //构造方法私有化
    private Singleton2(){}
    private static Singleton1 single = null;

    public static Singleton2 getInstance(){
        if(single == null){
            single = new Singleton2();
        }
        return single;
    }
}
//静态内部类实现
public class Singleton3{
    private Singleton3(){}

    private static class InnerObject{
        private static Singleton3 single = new Singleton3();
    }

    public static Singleton3 getInstance(){
        return single;
    }
}
//静态代码块实现
public class Singleton4{
    private Singleton4(){}

    private static Singleton4 single = null;
    static {
        single = new Singleton4();
    }
    public static Singleton4 getInstance(){
        return single;
    }
}

//工厂模式
//首先有个实体类
public class User{
    public String name;
    public String password;

}
//设定一个接口
public interface IUser{
    public void insert(User user);
}
//接口的实现类
public class IUserMysqlImpl implements IUser{
    @Override
    public void insert(User user){
        System.out.println(user.name+","+user.password);
    }
}
//假设有很多的实现类，每次实现都需要使用一个实现类
//创建一个工厂用来生产实体类
public class IUserFactory{
    //写个生产实现类的方法
    public static IUser getIUserImpl(){
        
        return new IUserMysqlImpl();
    }
}
//实现方式， IUser = IUserFactory.getIUserImpl();
//之后如果需要修改只需要改工厂的生产方式。实现方式不变。


//观察者模式
//首先有四个角色，抽象观察者、抽象观察的事物、具体的观察者、具体的事物。
//抽象的观察者
public interface watcher{
    public void update(String str);
}

//抽象的事物
public interface watched{
    public void addWatcher(Watcher watcher);
    public void deleteWatcher(Watcher watcher);
    public void notifyWatcher(String str);
}

//具体的观察者
public class watcherImpl implements watcher{
    @Override
    public void update(String str){
        System.out.println(str);
    }
}

//具体的事物
public class watchedImpl implements watched{
    //存放观察者
    private List<Watcher> list = new ArrayList<Watcher>();

    @Override
    public void addWatcher(Watcher watcher){
        list.add(watcher);
    }

    @Override
    public void deleteWatcher(Watcher watcher){
        list.remove(watcher);
    }

    @Override
    public void notifyWatcher(String str){
        for(Watcher watcher:list){
            watcher.update(str);
        }
    }
}
//测试类
public class Test{
    public static void main(String args[]){
        //设立一个观察事物
        Watched girl = new watchedImpl();
        //设立观察者
        Watcher watcher1 = new watcherImpl();
        Watcher watcher2 = new watcherImpl();
        Watcher watcher3 = new watcherImpl();

        //将观察者放入观察的事物中
        girl.addWatcher(watcher1);
    }
}
//代理模式
//静态代理模式
public interface Person{
    public void submit();
}
public class PersonImpl implements Person{
    public void submit(){
        System.out.println("提交作业");
    }
}
public class PersonProxy implements Person{
    private Person person;
    public PersonProxy(Person person){
        this.person = person;
    }
    @Override
    public void submit(){
        person.submit();
    }
}
public class Test{
    //具体类实现
    PersonImpl  target = new PersonImpl();

    PersonProxy proxy = new PersonProxy(target);
    proxy.submit();
}
//动态代理
//依旧是一个接口、一个接口的实现类
//接着是动态代理工厂的实现
//在java的java.lang.reflect包下有个Proxy类和InvocationHandler接口可以生成JDK动态代理类以及动态代理对象
//newProxyInstance(Person.class.getClassLoader(), new Class<?>[]{Person.class}, stuHandler);方法中，